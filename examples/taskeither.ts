import { pipe } from "fp-ts/lib/function"
import * as TE from "fp-ts/lib/TaskEither"

type ResponseFetchError = {
  type: "RESPONSE_FETCH_ERROR"
  error: unknown
}
type Response400Error = {
  type: "RESPONSE_400_ERROR"
  error: number
}
type L = ResponseFetchError | Response400Error

interface Response {
  status: number
  body: string
}

const getUser = (userId: string) =>
  pipe(
    TE.tryCatch(
      async () => ({
        status: 200,
        body: userId,
      }),
      (e) => ({
        type: "RESPONSE_FETCH_ERROR" as const,
        error: e,
      })
    ),
    TE.chainW((res) =>
      res.status >= 400 && res.status < 500
        ? TE.left({
            type: "RESPONSE_400_ERROR" as const,
            error: res.status,
          })
        : TE.right(res)
    )
  )

const taskeither = pipe(
  getUser("user.123"),
  TE.chain(() => getUser("user.234")),
  TE.map((user) => console.log(user))
)

taskeither()
