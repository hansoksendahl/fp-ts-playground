import { assertRight } from '../utils'
import { add5Traverse } from './01-traverse'

describe('01-traverse', () => {
  it('add5Traverse returns a task with the proper calculation', async () => {
    const result = await add5Traverse([1, 2, 3])()
    assertRight(result)
    expect(result.right).toStrictEqual([6, 7, 8])
  })
})
