import { add5Sequence } from './00-sequence'

describe('00-sequence', () => {
  it('add5Sequence returns a task with the proper calculation', async () => {
    const result = await add5Sequence([1, 2, 3])()
    expect(result).toStrictEqual([6, 7, 8])
  })
})
