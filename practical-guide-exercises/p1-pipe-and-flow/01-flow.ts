/**
 * Be sure to read part 1 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-1
 *
 * Exercise:
 *  - Modify goodDayMany to use flow instead of pipe
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 01-flow.ts
 *
 * Testing this file.
 *  - npm run test pipe-and-flow
 */

import { pipe } from 'fp-ts/lib/function'
import * as TE from 'fp-ts/lib/TaskEither'
import { prompt, runIfCli } from '../utils'
import { GreetingFunction } from './00-pipe'

const hello = (name: string) => `Hello ${name}!`
const appendGoodDay = (str: string) => `${str}Good day!`
const appendSpace = (str: string) => str + ' '

const greetMany = (names: string[], greetingFunction: GreetingFunction) =>
  names.map(greetingFunction)

// Modify this function to use flow instead of pipe
export const goodDayMany = (names: string[]) =>
  greetMany(names, name => pipe(hello(name), appendSpace, appendGoodDay))

// No need to modify below here, for running this file
pipe(
  prompt([
    { name: 'yourName', message: 'What is your name?' },
    { name: 'friendsName', message: 'What is a name of a friend?' },
  ]),
  TE.map(answers => console.dir(goodDayMany([answers.yourName, answers.friendsName]))),
  runIfCli(module)
)
