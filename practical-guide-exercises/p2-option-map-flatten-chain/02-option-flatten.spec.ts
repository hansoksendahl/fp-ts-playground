import * as O from 'fp-ts/lib/Option'
import { multiplyPositiveNumberBy5 } from './02-option-flatten'

describe('option-flatten', () => {
  it('multiplyPositiveNumberBy5 returns an option with the proper calculation', () => {
    expect(multiplyPositiveNumberBy5(10)).toStrictEqual(O.some(50))
  })
  it('multiplyPositiveNumberBy5 returns an option when the input is negative', () => {
    expect(multiplyPositiveNumberBy5(-5)).toStrictEqual(O.none)
  })
  it('multiplyPositiveNumberBy5 returns an option none if input is undefined', () => {
    expect(multiplyPositiveNumberBy5()).toStrictEqual(O.none)
  })
})
