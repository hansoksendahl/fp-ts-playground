/**
 * Be sure to read part 2 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-2
 *
 * Exercise:
 *  - Modify add5Subtract1 to return Option<number> instead of (number | undefined)
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 01-option-map.ts
 *
 * Testing this file.
 *  - npm run test 01-option-map
 */

import { pipe } from 'fp-ts/lib/function'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../utils'

const add5 = (number: number) => number + 5
const subtract1 = (number: number) => number - 1

// TODO Modify add5Subtract1 to return Option<number> instead of (number | undefined)
// TODO by converting the number variable to an Option and using O.map to call add5
// TODO and subtract1
export const add5Subtract1 = (number?: number): number | undefined =>
  pipe(
    number,
    num => (num !== undefined ? add5(num) : undefined),
    num => (num !== undefined ? subtract1(num) : undefined)
  )

// No need to modify below here, for running this file
const logAdd5Subtract1 = (number?: number) =>
  pipe(console.dir(`add5Subtract1(${number}):`), () => console.dir(add5Subtract1(number)))

pipe(
  T.fromIO(() => pipe(logAdd5Subtract1(10), () => logAdd5Subtract1())),
  runIfCli(module)
)
