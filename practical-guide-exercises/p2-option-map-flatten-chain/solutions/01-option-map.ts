/**
 * Be sure to read part 2 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-2
 *
 * Exercise:
 *  - Modify add5Subtract1 to return Option<number> instead of (number | undefined)
 */

import { pipe } from 'fp-ts/lib/function'
import * as O from 'fp-ts/lib/Option'
import * as T from 'fp-ts/lib/Task'
import { runIfCli } from '../../utils'

const add5 = (number: number) => number + 5
const subtract1 = (number: number) => number - 1

// TODO Modify add5Subtract1 to return Option<number> instead of (number | undefined)
export const add5Subtract1 = (number?: number): O.Option<number> =>
  pipe(O.fromNullable(number), O.map(add5), O.map(subtract1))

// No need to modify below here, for running this file
const logAdd5Subtract1 = (number?: number) =>
  pipe(console.dir(`add5Subtract1(${number}):`), () => console.dir(add5Subtract1(number)))
pipe(
  T.fromIO(() => pipe(logAdd5Subtract1(10), () => logAdd5Subtract1())),
  runIfCli(module)
)
