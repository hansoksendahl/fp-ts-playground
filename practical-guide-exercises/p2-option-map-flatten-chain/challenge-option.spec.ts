import * as O from 'fp-ts/lib/Option'
import { add5Subtract1 } from './challenge-option'

describe('option-challenge', () => {
  it('add5Subtract1 challenge returns an option with the proper calculation', () => {
    const numObj = { number: 10 }
    expect(add5Subtract1(numObj)).toStrictEqual(O.some({ number: 14 }))
  })
  it('add5Subtract1 challenge returns an option none if input is an empty object', () => {
    expect(add5Subtract1({})).toStrictEqual(O.none)
  })
})
