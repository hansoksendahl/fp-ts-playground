import { pipe } from 'fp-ts/lib/function'
import { lookupIsNone } from './00-array'

describe('array', () => {
  it('lookupIsNone returns true for a negative array index', () => {
    expect(pipe([0, 1, 2], lookupIsNone(-1))).toStrictEqual(true)
  })
  it('lookupIsNone returns true for an index that is out of bounds', () => {
    expect(pipe([0, 1, 2], lookupIsNone(20))).toStrictEqual(true)
  })
  it('lookupIsNone returns false if the index is in the array', () => {
    expect(pipe([0, 1, 2], lookupIsNone(0))).toStrictEqual(false)
  })
})
