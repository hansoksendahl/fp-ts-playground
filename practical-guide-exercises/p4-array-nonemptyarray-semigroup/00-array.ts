/**
 * Be sure to read part 4 before attempting these exercises! https://rlee.dev/practical-guide-to-fp-ts-part-4
 *
 * Exercise:
 *  - Modify lookupIsNone by adding an Array method that returns Option<T> before the call to O.isNone
 *
 * To run this file.
 *  - Ensure you are in this directory in the terminal
 *  - ts-node 00-array.ts
 *
 * Testing this file.
 *  - npm run test 00-array
 */
import * as A from 'fp-ts/lib/Array'
import * as O from 'fp-ts/lib/Option'
import * as T from 'fp-ts/lib/Task'
import { ap } from 'fp-ts/lib/Identity'
import { pipe } from 'fp-ts/lib/function'

import { runIfCli } from '../utils'

// TODO Modify lookupIsNone by adding an Array method that returns Option<T> before the call to O.isNone
export const lookupIsNone = (index: number) => <T>(arr: T[]): boolean =>
  pipe(
    arr[index],
    O.isNone
  )

// No need to modify below here, for running this file
const logResult = <T>(arr: T[]) => (number: number) =>
  pipe(console.dir(`lookupIsNone(${number}):`), () =>
    console.dir(pipe(arr, lookupIsNone(number)))
  )

pipe(
  T.fromIO(() =>
    pipe(
      pipe(logResult, ap(['🦁', '🐯', '🐻']), ap(1)),
      () => pipe(logResult, ap([]), ap(1))
    )
  ),
  runIfCli(module)
)