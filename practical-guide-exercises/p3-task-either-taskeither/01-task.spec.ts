import { add5Task } from './01-task'

describe('01-task', () => {
  it('add5Task returns an task with the proper calculation', async () => {
    const result = await add5Task(10)()
    expect(result).toStrictEqual(15)
  })
})
