import * as E from 'fp-ts/lib/Either'
import { assertLeft } from '../utils'
import { multiplyPositiveNumberBy5 } from './02-either'

describe('either', () => {
  it('multiplyPositiveNumberBy5 returns an either right with the proper calculation', () => {
    expect(multiplyPositiveNumberBy5(10)).toStrictEqual(E.right(50))
  })
  it('multiplyPositiveNumberBy5 returns an either left when the input is negative', () => {
    const result = multiplyPositiveNumberBy5(-5)
    assertLeft(result)
    expect(result.left.type).toStrictEqual('POSITIVE_NUMBER_ERROR')
  })
})
