# Fp-ts Playground

This repo is for learning fp-ts.

## Getting started

Install the required dependencies by executing `npm install`

## Practical Guide to Fp-ts exercises

Exercises meant to help reinforce the fp-ts concepts explained in the Practical Guide to fp-ts (https://rlee.dev/practical-guide-to-fp-ts-part-1)

Get started here: [practical-guide-exercies](practical-guide-exercises)

## Examples

There are some commonly used monad examples in this folder.
